class Solution {
    public boolean judgeCircle(String moves) {
        int x = 0;
        int y = 0;
        if (moves == null && moves == "")
            return true;
        
        for(char c: moves.toCharArray()) {
            if(c == 'U') x += 1;
            if(c == 'D') x -= 1;
            if(c == 'R') y += 1;
            if(c == 'L') y -= 1;
            
        }

        return x == 0 && y == 0 ? true : false;
    }
}