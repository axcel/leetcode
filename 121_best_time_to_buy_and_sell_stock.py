class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        maxProf = 0
        best_buy = prices[0]
        for i in range(1, len(prices)):
            if prices[i] < best_buy:
                best_buy = prices[i]
            else:
                profit = prices[i] - best_buy
                if maxProf < profit:
                    maxProf = profit
        return maxProf