# def flipAndInvertImage(image):
#     for img in image:
#         for j in range(len(img), 0, -1):
#             if img[j-1] == 1:
#                 img[j-1] = 0
#             else:
#                 img[j-1] = 1
#         img.reverse()
#     return image

def flipAndInvertImage(image):
    for img in image:
        for j in range(0, len(img)):
            if img[j] == 1:
                img[j] = 0
            else:
                img[j] = 1
        img.reverse()
    return image

l = [[1,1,0],[1,0,1],[0,0,0]]
print(flipAndInvertImage(l))