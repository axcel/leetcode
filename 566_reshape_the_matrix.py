from typing import List


class Solution:
    def matrixReshape(self, nums: List[List[int]], r: int, c: int) -> List[List[int]]:
        one_row = []
        matrix = []
        for i in range(len(nums)):
            for j in range(len(nums[0])):
                one_row.append(nums[i][j])
                
        if r*c == len(one_row):
            for k in range(0, len(one_row), c):
                matrix.append(one_row[k:k+c])
            return matrix
        else:
            return nums


s = Solution()
s.matrixReshape([[1,2],[3,4]], 4, 1)