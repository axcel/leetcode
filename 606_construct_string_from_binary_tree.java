//You need to construct a string consists of parenthesis and integers from a binary tree with the preorder traversing way.

//The null node needs to be represented by empty parenthesis pair "()". And you need to omit all the empty parenthesis pairs that don't affect the one-to-one mapping relationship between the string and the original binary tree.


class Solution {

    void solve(TreeNode root,TreeNode parent, StringBuilder sb){
        if(root == null){
            if(parent.left == null && parent.right != null) sb.append("()");
            return;
        }
        sb.append("("+root.val);
        solve(root.left, root, sb);
        solve(root.right, root, sb);
        sb.append(")");
    }

    public String tree2str(TreeNode t) {
        StringBuilder sb = new StringBuilder("");
        if(t == null) return "";

        sb.append(t.val);
        solve(t.left, t, sb);
        solve(t.right, t, sb);
        return sb.toString();
    }
}
