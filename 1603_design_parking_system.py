class ParkingSystem:

    def __init__(self, big: int, medium: int, small: int):
        self.map = {1: big, 2: medium, 3: small}
       
    def addCar(self, carType: int) -> bool:
        if self.map.get(carType) != 0:
            self.map[carType] = self.map.get(carType) - 1
            return True
        return False