from utils import TreeNode, creatBTree

class Solution:

    def increasingBST(self, root: TreeNode) -> TreeNode:
        self.list=[]
        if not root: return
        
        self.inorder(root)
        
        for i in range(len(self.list)-1):
            self.list[i].left = None
            self.list[i].right = self.list[i+1]
        
        # self.list[-1].left=None
        # self.list[-1].right=None
        
        return self.list[0]
        
    def inorder(self, root):
        if root == None: return
        
        if(root.left): self.inorder(root.left)
        self.list.append(root)
        if (root.right): self.inorder(root.right)




s = Solution()
root = creatBTree([5,3,6,2,4,None,8,1,None,None,None,7,9])

print(s.increasingBST(root))
