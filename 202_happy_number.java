class Solution {
    public boolean isHappy(int n) {
        HashSet<Integer> set = new HashSet<Integer>();
        while (true) {
            Integer value = getValue(n);
            if (value == 1) {
                return true;
            }
            if (!set.add(value)) {
                return false;
            }
            n = value;
        }
        
    }
    
    public static Integer getValue(int n) {
        Integer value = 0;
        while (n != 0 ) {
            int i = n % 10;
            value += i*i;
            n /= 10;
        }
        return value;
    }
}