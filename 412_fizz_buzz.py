# Write a program that outputs the string representation of numbers from 1 to n.

# But for multiples of three it should output “Fizz” instead of the number and for the multiples of five output “Buzz”. For numbers which are multiples of both three and five output “FizzBuzz”.

# Example:
# n = 15,
# Return:
# [
    # "1",
    # "2",
    # "Fizz",
    # "4",
    # "Buzz",
    # "Fizz",
    # "7",
    # "8",
    # "Fizz",
    # "Buzz",
    # "11",
    # "Fizz",
    # "13",
    # "14",
    # "FizzBuzz"
# ]

#TODO need optimize
def fizzBuzz(self, n: int) -> List:
    l = []
    if n == 1:
        return ["1"]
    for i in range(1, n+1):
        if i%3 == 0 and i%5 == 0:
            el = 'FizzBuzz'
        elif i%3 == 0:
            el = 'Fizz'
        elif i%5 == 0:
            el = 'Buzz'
        else:
            el = i
        l.append(el)
    return l


class Solution:
    def fizzBuzz(self, q: int) -> List[str]:
        n = [i for i in range(1, q+1)]
        for i in range(len(n)):
            if n[i] % 15 == 0:
                n[i] = "FizzBuzz"
            elif n[i] % 5 == 0:
                n[i] = 'Buzz'
            elif n[i] % 3 == 0:
                n[i] = 'Fizz'
            else:
                n[i] = str(n[i])
        return n
