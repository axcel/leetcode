class Solution {
    public int strStr(String haystack, String needle) {
        if (needle.length() == 0) {return 0;}
        int idx = haystack.indexOf(needle);
        return idx;
    }
}
