// great solution here https://www.youtube.com/watch?v=ttHAE_l8qBo

class Solution {
    public int[] replaceElements(int[] arr) {
        
        int[] new_arr = new int[arr.length];
        int[] copy_arr = arr;
        for (int i=0; i<arr.length-1; i++) {
            copy_arr[i] = 0;
            
            new_arr[i] = arrayMax(copy_arr);
        }
        new_arr[arr.length-1] = -1;
        return new_arr;
    }
    
    public int arrayMax(int[] arr) {
        int max = 0;
        for(int cur: arr)
            max = Math.max(cur, max);
        return max;
    }
}