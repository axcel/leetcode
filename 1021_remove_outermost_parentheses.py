def removeOuterParentheses(S: str) -> str:
    stack = []
    res = ''
    
    for i in range(len(S)):
        if S[i] == '(' and len(stack) == 0:
            stack.append(S[i])
            start = i
            continue

        if S[i] == '(':
            stack.append(S[i])      
        if S[i] == ')':
            stack.pop()
        if not stack:
            res += S[start+1: i]

    print(res)
    return res


removeOuterParentheses("(()())(())")
removeOuterParentheses("(()())(())(()(()))")
removeOuterParentheses("()()")