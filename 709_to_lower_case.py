# Implement function ToLowerCase() that has a string parameter str, and returns the same string in lowercase
# example
# Input: "LOVELY"
# Output: "lovely"


def toLowerCase(sefl, str) -> str:
    return ''.join(i.lower() if i.upper() else i for i in str)


def toLowerCase(self, str) -> str:
    if len(str) == 0 or str.islower(): return str

    for i in str:
        if str[i].isupper():
           str = str[:i] + str[i].lower() + str[i+1:]
    return ''.join(str) 