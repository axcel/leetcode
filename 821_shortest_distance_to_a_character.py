def shortestToChar(s: str, c: str):
  
        left_to_right = [float('inf')] * len(s)
        right_to_left = [float('inf')] * len(s)
    
        rDis = float('inf')
    
        # left to right distance
        for i in range(len(s)):
            if s[i] == c:
                rDis = 0
                left_to_right[i] = rDis
            else:
                if rDis != float('inf'):
                    rDis+=1
                    left_to_right[i] = rDis
                
        rDis = float('inf')
        # right to left distance
        for i in range(len(s)-1, -1, -1):
            if s[i] == c:
                rDis = 0
                right_to_left[i] = rDis
            else:
                if rDis != float('inf'):
                    rDis +=1
                    right_to_left[i] = rDis
                    
        ans = []    
        for i in range(len(left_to_right)):
            ans.append(
                min(
                    left_to_right[i], right_to_left[i]
                )
            )
        
        return ans

shortestToChar("aaba", "b")