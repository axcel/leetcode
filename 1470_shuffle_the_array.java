class Solution {
    public int[] shuffle(int[] nums, int n) {
        int len = nums.length;
        int[] arr = new int[len];
        int arrInx = 0;
        for (int i=0; arrInx<len; i++) {
            arr[arrInx++] = nums[i];
            arr[arrInx++] = nums[n++];
        }
        return arr;
    }
}
