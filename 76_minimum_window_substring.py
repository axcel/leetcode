def minWindow(s: str, t: str) -> str:
    
    if (len(s) < len(t)): return ""

    table = {}
    ans = ""
    str_len = len(s)+1
    
    for i in t:
        if (table.get(i)):
            table[i] += 1
        else:
            table[i] = 1
        
    begin: int = 0
    end: int = 0
    counter = len(table)
    while(end < len(s)):
        
        if (s[end] in table.keys()):
            table[s[end]] -= 1
            if (table[s[end]] == 0):
                counter -= 1

        end += 1

        while(counter == 0):

            if (end - begin < str_len):
                str_len = end - begin
                ans = s[begin:end]

            if (s[begin] in table):
                table[s[begin]] += 1
                if(table[s[begin]]>0): counter +=1
            
            begin +=1
    return ans

    

print(minWindow("ADOBECODEBANC", "ABC")) # BANC
print(minWindow("ab", "b")) # b
print(minWindow("ab", "a")) # a
print(minWindow("a", "aa")) # ""
print(minWindow("aa", "aa")) # "aa"