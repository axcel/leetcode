class Solution:
    def countGoodSubstrings(self, s: str) -> int:
        ans = 0

        if len(s) < 3:
            return 0
        
        for i in range(len(s)+1-3):
            word = s[i:i+3]
            if len(set(word)) == 3:
                ans += 1
                
        return ans
