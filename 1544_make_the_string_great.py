class Solution:
    def makeGood(self, s: str) -> str:
        st = []
        for i in s:
            st.append(i)
            if len(st) > 1:
                if self.isBad(st[-1], st[-2]):
                    st.pop()
                    st.pop()
        return ''.join(st)
    
    def isBad(self, x, y):
        delta = abs(ord('A')-ord('a'))
        return abs(ord(x)-ord(y)) == delta