class Solution:
    def isUnivalTree(self, root: TreeNode) -> bool:
        
        queue = []
        value = root.val
        
        queue.append(root)
        
        while(queue):
            node = queue.pop()
            
            if node.val != value:
                return False
            else:
                if node.left:
                    queue.append(node.left)
                if node.right:
                    queue.append(node.right)
                    
        return True