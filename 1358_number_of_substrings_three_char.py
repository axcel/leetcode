class Solution:
    def numberOfSubstrings(self, s: str) -> int:
        start, matched = 0, 0
        freq_map = {}      
            
        for end in range(len(s)):
            right = s[end]

            freq_map[right] = freq_map.get(right, 0) + 1

                    
            while len(freq_map) == 3:
                left = s[start]
                freq_map[left] = freq_map.get(left, 0) - 1
                if freq_map[left] <= 0:
                    del freq_map[left]
                start += 1
                
            matched += start   
        return matched