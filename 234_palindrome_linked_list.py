# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def isPalindrome(self, head: ListNode) -> bool:
        slow = head
        fast = head
        
        while (fast != None and fast.next != None):
            slow = slow.next
            fast = fast.next.next
        
        rev_slow = self.reverseLinkedList(slow)
        
        while rev_slow:
            print(rev_slow.val)
            if (rev_slow.val != head.val):
                return False
            rev_slow = rev_slow.next
            head = head.next
        return True
    
    def reverseLinkedList(self, head: ListNode) -> ListNode:
        prev = None
        while head:
            tmp = head.next
            head.next = prev
            prev = head
            head = tmp
        
        return prev