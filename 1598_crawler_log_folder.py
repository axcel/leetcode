from typing import List


def minOperations(logs: List[str]) -> int:
    stack = []
    
    for log in logs:
        if (log == '../' and stack):
            stack.pop()
        elif (log not in ['./', '../']):
            stack.append(log)

    return len(stack)

a = ["./","../","./"]

minOperations(a)