class Solution:
    def reverseWords(self, s: str) -> str:
        ans = []
        split_str = s.split()
        for word in split_str:
            ans.append(word[::-1])
        return " ".join(ans)