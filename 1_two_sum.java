class Solution {
    public int[] twoSum(int[] nums, int target) {
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i=0; i<nums.length; i++) {
            map.put(nums[i], i);
        }

        for (int i=0; i< nums.length; i++) {
            int find = target - nums[i];
            if (map.contains(find) && map.contains(find) != i) {
                return new int[]{map.get(find), i};
            }
        }
        throw new IllegalArgumenException("No find solution");
    }
}