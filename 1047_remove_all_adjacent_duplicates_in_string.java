class Solution {
    public String removeDuplicates(String S) {
        ArrayDeque<Character> stack = new ArrayDeque<>();
        String in = "";
        String out = "";
        for(int i=0; i<S.length(); i++) {
            if(!stack.isEmpty() && stack.peek() == S.charAt(i)) {
                stack.pop();
            } else {
                stack.push(S.charAt(i));
            }
        }
        
        StringBuilder sb = new StringBuilder();
        for(char s:stack){
            sb.append(Character.toString(s));
        }
        return sb.reverse().toString();
    
    }
}