from typing import List
import math


def findMaxAverage(nums: List[int], k: int) -> float:
    start, _sum = 0, 0
    average = -math.inf

    for end in range(len(nums)):
        _sum += nums[end]

        if end >= k - 1:
            average = max(average, _sum/k)
            _sum -= nums[start]
            start += 1

    return average

findMaxAverage([1,12,-5,-6,50,3], 4)