class Solution:
    def restoreString(self, s: str, indices: List[int]) -> str:
        map = {}
        ans = ''
        
        for i in range(len(indices)):
            idx = indices[i]
            map[idx] = s[i]

        
        for i in range(len(indices)):
            ans += map.get(i)
        
        return ans
        # hint l[index] = alpha