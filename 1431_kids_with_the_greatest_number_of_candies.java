class Solution {
    public List<Boolean> kidsWithCandies(int[] candies, int extraCandies) {
        
        int max = 0;
        List<Boolean> arr = new ArrayList();
        if (candies.length == 0) {
            return arr;
        }
        
        for (int curr: candies) {
            max = Math.max(curr, max);
        }
        
        for (int i=0; i<candies.length; i++) {
            if (candies[i] + extraCandies >= max) {
                arr.add(true);
            } else {
            arr.add(false);
            }
        }
        return arr;
    }
}