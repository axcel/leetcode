# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def maxDepth(self, root: TreeNode) -> int:
        stack = []
        stack.append(root)
        l_subtree = 0
        r_subtree = 0
        ans = 1
        if root == None:
            return ans
        
        while(len(stack) != 0):
            node = stack.pop()
            
            if root.left != None:
                l_subtree +=1
                stack.append(root.left)
            if root.right != None:
                r_subtree +=1
                stack.append(root.right)
        return ans + max(l_subtree, r_subtree)


class Solution:

    def maxDepth(self, root: TreeNode) -> int:
 
        if root == None:
            return 0
        return 1+max(self.maxDepth(root.left), self.maxDepth(root.right))