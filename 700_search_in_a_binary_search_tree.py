class Solution:
    def searchBST(self, root: TreeNode, val: int) -> TreeNode:
        stack = []
        stack.append(root)
        
        while stack:
            node = stack.pop(0)
            
            if node.val == val: return node
            
            if node.left:
                stack.append(node.left)
                
            if node.right:
                stack.append(node.right)
        return None

# def searchBST(root, val):
#     if root == None: 
#         return None
    
#     if root.val == val: 
#         print(root)
#         return root
    
#     if (root.left != None):
#         return searchBST(root.left, val)
#     if (root.right != None):
#         return searchBST(root.right, val)

# from TreeNode import creatBTree


# rt = creatBTree([4,2,7,1,3])

# print(searchBST(rt, 2))