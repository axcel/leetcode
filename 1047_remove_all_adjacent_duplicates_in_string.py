class Solution:
    def removeDuplicates(self, S: str) -> str:
        stack = []

        for i in range(len(S)):
            if len(stack) == 0:
                stack.append(S[i])
            elif stack[-1] == S[i]:
                stack.pop()
            else:
                stack.append(S[i])
        return ''.join(stack)