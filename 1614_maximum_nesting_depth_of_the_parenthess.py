class Solution:
    def maxDepth(self, s: str) -> int:
        if not s: return 0
        stack = []
        maxLen = 0
        for i in range(0, len(s)):
            if s[i] == '(':
                stack.append(s[i])
            if s[i] == ')':
                maxLen = max(maxLen, len(stack))
                stack.pop()
        return maxLen