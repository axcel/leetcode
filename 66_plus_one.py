# Given a non-empty array of digits representing a non-negative integer, plus one to the integer.

# The digits are stored such that the most significant digit is at the head of the list, and each element in the array contain a single digit.

# You may assume the integer does not contain any leading zero, except the number 0 itself.

# Example 1:

# Input: [1,2,3]
# Output: [1,2,4]
# Explanation: The array represents the integer 123.


def plusOne(digits: List) -> List:
    j = len(digits) -1
    digit = 0
    for i in digits:
        digit += i*(10**j)
        j -= 1
    digit += 1
    return revers(digit)


def revers(lst: List) -> List:
    l = []
    while lst:
        l += [lst%10]
        lst //= 10
    return l[::-1]
