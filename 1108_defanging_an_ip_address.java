class Solution {
    public String defangIPaddr(String address) {
        if (address.length() > 7 || address == null) {
            return address;
        }
        String ret="";
        String[] parts = address.split("\\.");
        for (int i = 0; i<parts.length; i++) {
            if (i==3) { 
                ret += parts[i];
            } else {
                ret += parts[i] + "[.]";
            }
        }
        return ret;
    }
}