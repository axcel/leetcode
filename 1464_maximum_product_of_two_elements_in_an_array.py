from typing import List


class Solution:
    def maxProduct(self, nums: List[int]) -> int:
        tmp = -1
        for i in range(len(nums)-1):
            for j in range(i+1, len(nums)):
                tmp = max(
                    (nums[i]-1)*(nums[j]-1),
                    tmp
                )


s = Solution()
print(s.maxProduct([1,5,4,5]))


class Solution:
    def maxProduct(self, nums: List[int]) -> int:
        m1 = max(nums)
        nums.remove(m1)
        return (m1-1) * (max(nums)-1)