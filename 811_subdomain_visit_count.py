class Solution:
    def subdomainVisits(self, cpdomains: List[str]) -> List[str]:
        map = {}
        
        ans = []
        
        for domain in cpdomains:
            visit, domains = domain.split(" ")
            for d in self.listDomains(domains):
                map[d] = map.get(d, 0) + int(visit)
                
        for k, v in map.items():
            ans.append(
                str(v) + " " + k
            )
            
        # return ['{} {}'.format(k, v) for k,v in map.items()]
            
        return ans
    
    def listDomains(self, domains):
        l = []

        flags = domains.split('.')
        for i in range(len(flags)):
            l.append('.'.join(flags[i:]))
            
        return l