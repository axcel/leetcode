class Solution:
    def evalRPN(self, tokens: List[str]) -> int:
        stack = []
        
        for i in tokens:
            if i in ["+", "-", "*", "/"]:
                y = stack.pop()
                x = stack.pop()
                stack.append(self.evalExpress(x, y, i))
            else:
                stack.append(int(i))
        return stack[-1]
    
    def evalExpress(self, x, y, token):
        if token == '+': return x + y
        if token == '/': return int(x / y)
        if token == '-': return x - y
        if token == '*': return x * y