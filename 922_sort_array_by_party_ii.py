class Solution:
    def sortArrayByParityII(self, nums: List[int]) -> List[int]:
        ans = [0] * len(nums)
        
        even = 0
        for i in range(0, len(nums)):
            if nums[i] % 2 == 0:
                ans[even] = nums[i]
                even += 2
        
        odd = 1
        for i in range(len(nums)):
            if nums[i] % 2 != 0:
                ans[odd] = nums[i]
                odd += 2

        return ans