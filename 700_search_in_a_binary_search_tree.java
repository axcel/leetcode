class Solution {
    public TreeNode searchBST(TreeNode root, int val) {
        Deque<TreeNode> stack = new ArrayDeque<>();
        stack.push(root);
        
        while(!stack.isEmpty()) {
            TreeNode el = stack.pop();
            if (el != null) {
                if (el.val > val) stack.push(el.left);
                else if (el.val < val) stack.push(el.right);
                else return el;   
            }
        }
        return null;
    }
}


class Solution {
    public TreeNode searchBST(TreeNode root, int val) {
        if(root == null) return null;
        if(root.val >  val) return searchBST(root.left, val);
        if(root.val <  val) return searchBST(root.right, val);
        return root;
    }
}