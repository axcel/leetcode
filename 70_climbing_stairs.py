class Solution:
    def climbStairs(self, n: int) -> int:
        # if n == 1: return 1
        # if n == 2: return 2
        
        # return  self.climbStairs(n-1) + self.climbStairs(n-2)

        # if n <= 1: return n
        # dp = [0] * int(n+1)
        # dp[1] = 1
        # dp[2] = 2
        # print(dp)
        # for i in range(3,n+1):
        #     dp[i] = dp[i-1] + dp[i-2]
            
        # return dp[-1]


s = Solution()

s.climbStairs(4)
# print(s.climbStairs(4))