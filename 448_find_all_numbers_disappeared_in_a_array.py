from typing import List


def findDisappearedNumbers(nums: List[int]) -> List[int]:

    uniq = set(nums)
    ans = []
    for i in range(1, len(nums)+1):
        if i not in uniq:
            ans.append(i)
    return ans

findDisappearedNumbers([4,3,2,7,8,2,3,1])
findDisappearedNumbers([1,1])