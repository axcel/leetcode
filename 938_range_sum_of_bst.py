# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def rangeSumBST(self, root: TreeNode, low: int, high: int) -> int:
        self.ans = 0
        self.dfs(root, low, high)
        return self.ans
        
        
    def dfs(self, root, low, high):
        if not root:
            return
        
        if low <= root.val <= high:
            self.ans += root.val
            
        if root.left:
            self.dfs(root.left, low, high)
            
                    
        if root.right:
            self.dfs(root.right, low, high)
            
            
#     def rangeSumBST(self, root: TreeNode, low: int, high: int) -> int:
#         stack = []
#         ans = 0
#         stack.append(root)
        
#         while (len(stack) !=0):
#             node = stack.pop()
            
#             if low <= node.val <= high:
#                 ans += node.val
                
#             if node.left: stack.append(node.left)
#             if node.right: stack.append(node.right)    
            
#         return ans