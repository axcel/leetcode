class Solution:
    def postorder(self, root: 'Node') -> List[int]:

        if root == None: return []

        stack = []
        stack.append(root)
        ans = []

        while(len(stack) != 0):
            node = stack.pop()

            for child in node.children:
                stack.append(child)
            ans.append(node.val)

        return ans[::-1]