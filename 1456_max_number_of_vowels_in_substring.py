class Solution:
    def maxVowels(self, s: str, k: int) -> int:
        
        vowels = ['a', 'e', 'i', 'o', 'u']
        vcount = 0
        mvc = 0
        
        for i in range(k):
            if s[i] in vowels:
                vcount += 1
                
        mvc = vcount
        start = 0
        for end in range(k, len(s)):
            if s[start] in vowels:
                vcount -= 1
                
            if s[end] in vowels:
                vcount += 1
                
            start += 1
            
            mvc = max(mvc, vcount)
            
        return mvc