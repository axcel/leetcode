class Solution {
    public List<String> subdomainVisits(String[] cpdomains) {
        Map<String, Integer> map = new HashMap<>();
        for (String cpdomain : cpdomains) {
            int hits = Integer.parseInt(cpdomain.substring(0, cpdomain.indexOf(" ")));
            String curr = cpdomain.substring(cpdomain.indexOf(" ") + 1);
            while (!curr.isEmpty()) {
                map.put(curr, map.getOrDefault(curr, 0) + hits);
                int idx = curr.indexOf(".");
                if (idx != -1) {
                    curr = curr.substring(idx + 1);
                } else {
                    curr = "";
                }
            }
        }
        List<String> l = new ArrayList<String>();
        for (String key : map.keySet()) {
            int count = map.get(key);
            l.add(count + " " + key);
        }
        return l;
    }
}
