class Solution:
    def numIdenticalPairs(self, nums: List[int]) -> int:
        map = {}
        out = 0
        for i in nums:
            map[i] = map.get(i, 0) + 1
            
        for v in map.values():
            out += ((v) * (v-1))//2
            
        return out