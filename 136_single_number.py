# Given a non-empty array of integers, every element appears twice except for one. Find that single one.

# Note:

# Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?

# Example 1:

# Input: [2,2,1]
# Output: 1

# Example 2:

# Input: [4,1,2,1,2]
# Output: 4

# !!!!! need refactor
def singleNumber(nums: List) -> int:
    d = {}
    for i in nums:
        if d.get(i):
            d[i] += 1
        else:
            d[i] = 1
    return sorted(d, key=d.get)[0]

class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        ans = 0
        for i in nums:
            ans ^= i
        return ans   
