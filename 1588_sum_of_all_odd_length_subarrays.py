from typing import List


class Solution:    
    def sumOddLengthSubarrays(self, arr: List[int]) -> int:
        
        lst = self.oddList(arr)
        ans = 0
        for i in lst:
            ans += self.sumByEl(i, arr)
        return ans
        
    def oddList(self, arr):
        odd_list = []
        
        for k in range(1, len(arr)+1):
            if k % 2 == 1:
                odd_list.append(k)
        print(odd_list)       
        return odd_list
    
    def sumByEl(self, k, array):
        s = 0
        if k == 1: 
            for i in range(0, len(array)):
                s += sum(array[i:i+k])
        elif k == len(array):
            s += sum(array)
        else:
            for i in range(0, len(array)+1-k):
                s += sum(array[i:i+k])
    
        return s


s = Solution()
s.sumOddLengthSubarrays([1,4,2,5,3])
print(s.sumOddLengthSubarrays([1,2]))
print(s.sumOddLengthSubarrays([10,11,12]))