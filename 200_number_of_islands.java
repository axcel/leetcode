class Solution {
    public int numIslands(char[][] grid) {
        // need add check grid not null or empty
        
        int ans = 0;
        for(int y=0; y < grid.length; y++) {
            for(int x=0; x < grid[0].length; x++) {
                if(grid[y][x] == '1') {
                    ans++;
                    dfs(grid, y, x);
                }
            }
        }
        System.out.println(grid.length);
        return ans;
    }
    
    private void dfs(char[][] grid, int y, int x) {
        if ( x < 0 || x >= grid[0].length || y < 0 ||  y >= grid.length || grid[y][x] != '1') return;
        
        grid[y][x] = '*';
        
        dfs(grid, y + 1, x);
        dfs(grid, y - 1, x);
        dfs(grid, y, x + 1);
        dfs(grid, y, x - 1);
    }
}
