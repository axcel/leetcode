class Solution {
    public boolean detectCapitalUse(String word) {
        int upperCount = 0;
        int lowerCount = 0;
        int wordLen = word.length();

        for (int i=0; i<wordLen; i++) {
            if (Character.isUpperCase(word.charAt(i))) {
                upperCount++;
            } else {
                lowerCount++;
            }

        }
        if (Character.isUpperCase(word.charAt(0)) && upperCount == 1 ||
            upperCount == wordLen ||
            lowerCount == wordLen)
        {
            return true;
        }
        return false;
        
    }
}