def diStringMatch(S: str):
    lo, hi = 0, len(S)
    ans = []
    
    for i in S:
        if i == 'I':
            ans.append(lo)
            lo += 1
        else:
            ans.append(hi)
            hi -= 1
    
    # why added lo ?! need debug
    return ans + [lo]


print(diStringMatch("DDI"))