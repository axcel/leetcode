class Solution {
    public int removeDuplicates(int[] nums) {
        int b = 0;
        for (int i=1; i<nums.length; i++) {
            if (nums[i] != nums[b]) {
                b++;
                nums[b] = nums[i];
            }
        }
        return b+1;
    }
}