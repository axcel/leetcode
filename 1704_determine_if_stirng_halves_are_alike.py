class Solution:
    def halvesAreAlike(self, s: str) -> bool:
        
        vowels =['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']
        a = s[:len(s)//2]
        b = s[len(s)//2:]
        
        vowels_a = 0
        vowels_b = 0
        
        for i in a:
            if i in vowels:
                vowels_a += 1
                
        for j in b:
            if j in vowels:
                vowels_b += 1
                
        return vowels_a == vowels_b