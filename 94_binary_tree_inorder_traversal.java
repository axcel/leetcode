/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    List<Integer> res = new ArrayList<Integer>();
    public List<Integer> inorderTraversal(TreeNode root) {
        if (root == null) {
            return res;
        }
        inorderTraversal(root.left);
        
        res.add(root.val);
        inorderTraversal(root.right);
        
        return res;
    }
}



class Solution {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> out = new ArrayList<>();
        if (root == null) return out;

        return inorderTree(root, out);
    }

    private List<Integer> inorderTree(TreeNode root, List treeList) {
        if (root == null) return treeList;


        inorderTree(root.left, treeList);
        treeList.add(root.val);
        inorderTree(root.right, treeList);
        return treeList;
    }
}
