class Solution {
    public String reverseWords(String s) {
        List<String> lst = new ArrayList<>();
        for (String str: s.split(" ")) {
            lst.add(reverse_str(str));
        }
        return String.join(" ", lst);
    }
    
    private String reverse_str(String str) {
        String word = "";
        for (int i=str.length()-1; i>=0; i--) {
            word += str.charAt(i);
        }
        return String.valueOf(word);
    }
}