class Solution:
    def intersection(self, nums1: List[int], nums2: List[int]) -> List[int]:
        ans = []
        
        if len(nums1) > len(nums2):
            tmp = nums2
            nums2 = nums1
            nums1 = tmp
            
        for i in nums1:
            if i in nums2 and i not in ans:
                ans.append(i)
                
        return ans


# NOTE
    def intersection(self, nums1: List[int], nums2: List[int]) -> List[int]:
       
        if len(nums1) > len(nums2):
            return self.intersection(nums1, nums2)
        else:
            return self.intersection(nums2, nums1)


    def intersection(self, n1, n2):
        ans = []
        for i in n1:
            if i in n2 and i not in ans:
                ans.append(i)
        return ans