// TODO need refactor

class Solution {
    public boolean checkRecord(String s) {        
        int countA = 0;
        int countL = 0;
        int maxCountL = 0;
        for (int i=0; i < s.length(); i++) {
            
            if (s.charAt(i) == 'A') {
                countA++;
                if (countA >= 2) {
                    return false;
                }
            }
            
            if (s.charAt(i) == 'L') {
                countL++;
                maxCountL = Math.max(countL, maxCountL);
                if (maxCountL >= 3) {return false;}
            } else {countL=0;}

        }
        return true;
    }
}