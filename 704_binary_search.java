class Solution {
    public int search(int[] nums, int target) {
        int first = 0;
        int last = nums.length-1;
        boolean found = false;
        
        while(first <= last && !found) {
            int mid = (first+last)/2;
            System.out.println(mid);
            if (nums[mid] == target) {
                found = true;
                return mid;
            }
            if (nums[mid] > target) {
                last = mid - 1;
            } else {
                first = mid + 1;
            }
        }
        return -1;
    }
}
