class Solution:
    def letterCombinations(self, digits: str) -> List[str]:
        map = {2:"abc", 3:"def", 4:"ghi", 5:"jkl", 6:"mno", 7:"pqrs", 8:"tuv", 9:"wxyz"}
        result = []
        
        if len(digits) == 0: return result
                     
        self.dfs(digits, map, result, "", 0)
        
        return result
        
        
    def dfs(self, digits, map, result, substr, index):
        if index == len(digits):
            result.append(substr)
            return
        
        
        digit = int(digits[index])
        letters = map.get(digit)
        
        
        for i in range(len(letters)):
            substr += letters[i]
            self.dfs(digits, map, result, substr, index+1)
            substr = substr[:-1]
        