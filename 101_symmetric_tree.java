class Solution {
    public boolean isSymmetric(TreeNode root) {
        if (root == null)
            return true;
        return isSym(root.left, root.right);
    }
    
    public boolean isSym(TreeNode left, TreeNode right) {
        if (left == null && right == null) {
            return true;
        } else if (left == null || right == null) {
            return false;
        }
        
        if (left.val != right.val) {
            return false;
        }
        
        if (!isSym(left.left, right.right)) {
            return false;
        }
        if (!isSym(left.right, right.left)) {
            return false;
        }
        
        return true;
    }
}