from typing import List


class Solution:
    def minSubArrayLen(self, target: int, nums: List[int]) -> int:
        win_start = 0
        _sum = 0
        _min = float('inf')
        
        for win_end in range(len(nums)):
            _sum += nums[win_end]
            
            while _sum >= target:
                _min = min(_min, win_end - win_start + 1)
                _sum -= nums[win_start]
                win_start += 1
                
        return _min if _min != float('inf') else 0