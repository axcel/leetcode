class Solution {
    public int maximumWealth(int[][] accounts) {
        int maxSum = 0;
        for (int i[]: accounts) {
            int sum = 0;
            
            for(int j: i) {
                sum += j;
            }
            maxSum = Math.max(maxSum, sum);
        }
        return maxSum;
    }
}
