class Solution {
    public boolean isAnagram(String s, String t) {
        if (s == null && t == null) {
            return false;
        }
        // String anagram = new StringBuilder(t).reverse().toString();
        HashMap mapS = getMapString(s);
        HashMap mapT = getMapString(t);
        if (mapS.equals(mapT)) 
            return true; 
        return false;
    }
    
    public HashMap getMapString(String s) {
        HashMap<Character, Integer> map = new HashMap<Character, Integer>();
        for (char c: s.toCharArray()) {
            if(map.get(c) == null) {
                map.put(c, 1);
            } else {
                map.put(c, map.get(c)+1);
            }
        }
        return map;
    }
}