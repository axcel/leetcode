# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def flatten(self, root: TreeNode) -> None:
        """
        Do not return anything, modify root in-place instead.
        """
        ans = []
        
        self.traversal(root, ans)
        
        for k in range(len(ans)-1):
            ans[k].left = None
            ans[k].right = ans[k+1]
        
        
    def traversal(self, node, ans):
        if not node:
            return
        
        ans.append(node)
        if node.left:
            self.traversal(node.left, ans)
            
        if node.right:
            self.traversal(node.right, ans)