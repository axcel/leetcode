class Solution {
    public int findNumbers(int[] nums) {
        
        if (nums == null || nums.length < 1) {
            throw new IllegalArgumentException("incorrect argument");
        }
        
        int counter = 0;
        for (int i=0; i<nums.length; i++) {
            if (countDigsts(nums[i]) % 2 == 0)
                counter++;
        }
        return counter;
    }
    
    public int countDigsts(int i) {
        return i == 0 ? 0 : 1 + countDigsts(i/10);
    }
}