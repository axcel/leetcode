from typing import List


class Solution:
    def generate(self, numRows: int) -> List[List[int]]:
        if numRows == 1: return [[1]]
        
        # triangle = [[1], [1,1]]
        triangle = [[1]]
        
        for i in range(2, numRows+1):
            row = self.genRow(triangle[-1], i)
            triangle.append(row)
        return triangle
            
    def genRow(self, n: List[int], row: int) -> List[List[int]]:
        tmp = [1]
        for i in range(len(n)-1):
            tmp.append(sum(n[i:i+2]))
            
        tmp.append(1)
        return tmp

s = Solution()
print(s.generate(5))