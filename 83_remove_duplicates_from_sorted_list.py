# Given a sorted linked list, delete all duplicates such that each element appear only once.

# Example 1:

# Input: 1->1->2
# Output: 1->2

# Example 2:

# Input: 1->1->2->3->3
# Output: 1->2->3


def deleteDuplicates(head):
    s = set()
    c = head
    while c:
        s.add(c.val)
        c = c.next
    return list(s)


# class Solution:
#     def deleteDuplicates(self, head: ListNode) -> ListNode:
#         slow = head
#         fast = head.next
#         while (fast and fast.next):
#             if fast.val == slow.val:
#                 slow = fast
#                 fast = fast.next
                
#         return slow



class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        slow = head
        fast = head.next
        while (fast and fast.next):
            if fast.val == slow.val:
                slow.next = fast.next
                fast = fast.next

            тупо выпала часть else
            else:
                slow = slow.next
                fast = fast.next
                
        return slow



# correct solution
class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        slow = head
        fast = head.next
        
        node = head
        while (node.next):
            if node.val == node.next.val:
                node.next = node.next.next
            else:
                node = node.next
                
        return head