def freqAlphabets(s: str) -> str:
    vowels = "abcdefghijklmnopqrstuvwxyz"
    
    ans = ''

    i = len(s) -1
    while i >= 0:
        if s[i] == '#':
            idx = int(s[i-2:i])
            ans = vowels[idx-1] + ans
            i -= 3
        else:
            idx = int(s[i])
            ans = vowels[idx-1] + ans
            i -= 1
    
    return ans

# print(freqAlphabets("10#11#12"))
print(freqAlphabets("1326#"))
# print(freqAlphabets("25#"))