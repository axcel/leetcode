class Solution {
    public int findMaxConsecutiveOnes(int[] nums) {
        int countOne = 0;
        int maxCount = 0;
        for (int i=0; i < nums.length; i++) {
            if(nums[i] == 1) {
                countOne++;
                maxCount = Math.max(countOne, maxCount);
            } else { countOne = 0; }
        }
        return maxCount;
    }
}