class ParkingSystem {
    int big;
    int medium;
    int small;
    // HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();

    public ParkingSystem(int big, int medium, int small) {
        this.big = big;
        this.medium = medium;
        this.small = small;
        // map.put(1, big);
        // map.put(2, medium);
        // map.put(3, small);
    }
    
    public boolean addCar(int carType) {
        // if (map.get(carType) > 0) {
        //     map.replace(carType, map.get(carType)-1);
        //     return true;
        // }
        // return false;
        if (carType == 1 && big > 0) {
            big--;
            return true;
        }
        if (carType == 2 && medium > 0) {
            medium--;
            return true;
        }
        if (carType == 3 && small > 0) {
            small--;
            return true;
        }
        return false;
    }
}