def smallerNumbersThanCurrent(nums):
    snums = sorted(nums)
    map = {}
    ans = []
    
    for i in range(0,len(snums)):
        if snums[i] not in map:
            map[snums[i]] = i
            
    for k in range(len(nums)):
        ans.append(map.get(nums[k]))
        
    return ans

print(smallerNumbersThanCurrent([8,1,2,2,3]))
print(smallerNumbersThanCurrent([6,5,4,8]))
print(smallerNumbersThanCurrent([7,7,7,7,7]))