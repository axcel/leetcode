class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        best_sum = float("-Inf")
        cur_sum = 0
        for i in nums:
            cur_sum = max(i, cur_sum + i)
            best_sum = max(best_sum, cur_sum)
        return best_sum
