class Solution:
    def romanToInt(self, s: str) -> int:
        map = {
            "I": 1,
            "V": 5,
            "X": 10,
            "L": 50,
            "C": 100,
            "D": 500,
            "M": 1000
        }
        
        out = 0
        
        for i in range(len(s)):
            if (i > 0 and map.get(s[i]) > map.get(s[i-1])):
                out += map.get(s[i]) - 2 * map.get(s[i-1])
            else:
                out += map.get(s[i])            
        return out