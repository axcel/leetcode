class Solution:
    def findComplement(self, num: int) -> int:
        invert = ''
        bin_num = self.tenToBin(num)
        for i in bin_num:
            invert += '0' if i == '1' else '1'
                
        return self.binToTen(invert)
        
    def tenToBin(self, t):
        s = ''
        while (t > 0):
            # n = t % 2
            s = str(t % 2) + s
            t = t // 2
            #s = str(n) + s
            
        # remove lead zeros
        for i in s:
            if i == '0':
                s = s[1:]
            else:
                break   
        return s
        
    def binToTen(self, b):
        dec = 0
        for i in range(len(b)):
            dec += int(b[i]) * pow(2, len(b)-i-1)
        return dec