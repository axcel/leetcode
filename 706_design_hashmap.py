class MyHashMap:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.size = 10000000 + 1
        self.hashMap = [None] * self.size
        

    def put(self, key: int, value: int) -> None:
        """
        value will always be non-negative.
        """
        self.hashMap[key] = value       
        

    def get(self, key: int) -> int:
        """
        Returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key
        """       
        if self.hashMap[key] is not None:
            return self.hashMap[key]
        return -1
        

    def remove(self, key: int) -> None:
        """
        Removes the mapping of the specified value key if this map contains a mapping for the key
        """
        self.hashMap[key] = None
        


obj = MyHashMap()
obj.put(1,1)
param_2 = obj.get(1)
obj.remove(1)        