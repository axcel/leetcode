class Solution {
    public char findTheDifference(String s, String t) {
        int sI = 0;
        int tI = 0;
        
        for (char c: s.toCharArray()) {
            sI += c;
        }
        
        for (char c: t.toCharArray()) {
            tI += c;
        }
        return (char) (tI-sI);
    }
}