/// notifyAll

class Foo {
    public int threadNum;
    public Foo() {
        this.threadNum = 1;
        
    }

    synchronized public void first(Runnable printFirst) throws InterruptedException {
        while(threadNum != 1) {
            wait();
        }
        // printFirst.run() outputs "first". Do not change or remove this line.
        printFirst.run();
        threadNum++;
        notifyAll();
    }

    synchronized public void second(Runnable printSecond) throws InterruptedException {
        while(threadNum != 2) {
            wait();
        }
        // printSecond.run() outputs "second". Do not change or remove this line.
        printSecond.run();
        threadNum++;
        notifyAll();
    }

    synchronized public void third(Runnable printThird) throws InterruptedException {
        while(threadNum != 3) {
            wait();
        }
        // printThird.run() outputs "third". Do not change or remove this line.
        printThird.run();
        threadNum++;
        notifyAll();
    }
}

//// ReentrantLock

class Foo {
    private int threadNum;
    private Lock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();
    public Foo() {
        this.threadNum = 1;
    }

    public void first(Runnable printFirst) throws InterruptedException {
        lock.lock();
        try {
            // printFirst.run() outputs "first". Do not change or remove this line.
            printFirst.run();
            threadNum++;
            condition.signalAll();
        } finally {
            lock.unlock();
        }
    }

    public void second(Runnable printSecond) throws InterruptedException {
        lock.lock();
        try {
            while (this.threadNum != 2) {
                condition.await();
            }
            // printSecond.run() outputs "second". Do not change or remove this line.
            printSecond.run();
            threadNum++;
            condition.signalAll();
        } finally {
            lock.unlock();
        }
        
    }

    public void third(Runnable printThird) throws InterruptedException {
                lock.lock();
        try {
            while (this.threadNum != 3) {
                condition.await();
            }
                // printThird.run() outputs "third". Do not change or remove this line.
                printThird.run();
            threadNum++;
            condition.signalAll();
        } finally {
            lock.unlock();
        }

    }
}

//// Semaphore

class Foo {
    private Semaphore s1 = new Semaphore(0);
    private Semaphore s2 = new Semaphore(0);
    public Foo() {
        
    }

    public void first(Runnable printFirst) throws InterruptedException {
        
        // printFirst.run() outputs "first". Do not change or remove this line.
        printFirst.run();
        s1.release();
    }

    public void second(Runnable printSecond) throws InterruptedException {
        s1.acquire();
        // printSecond.run() outputs "second". Do not change or remove this line.
        printSecond.run();
        s2.release();
    }

    public void third(Runnable printThird) throws InterruptedException {
        s2.acquire();
        // printThird.run() outputs "third". Do not change or remove this line.
        printThird.run();
    }
}