class Solution {
    public int findSpecialInteger(int[] arr) {
        int retVal=0;
        HashMap<Integer, Double> map = new HashMap();
        
        for (int i=0; i < arr.length; i++) {
            map.put(arr[i], map.getOrDefault(arr[i], 0.0)+1.0);
        }
        
        for(Map.Entry<Integer, Double> entry: map.entrySet()) {
            if (entry.getValue() >= (arr.length/4.0)) {
                retVal = entry.getKey();
            }
        }
        return retVal;
    }
}


class Solution {
    public int findSpecialInteger(int[] arr) {
        int retVal=0;
        HashMap<Integer, Double> map = new HashMap();
        
        for (int i=0; i < arr.length; i++) {
            map.put(arr[i], map.getOrDefault(arr[i], 0.0)+1.0);
            if (map.get(arr[i]) >= (arr.length/4.0)) {
                retVal = arr[i];
            }
        }
        
        return retVal;
    }
}
