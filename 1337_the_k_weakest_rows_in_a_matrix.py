from typing import List


class Solution:
    def kWeakestRows(self, mat: List[List[int]], k: int) -> List[int]:
        lst = []
        ans = []
        
        for row in range(len(mat)):
            sol = 0
            for i in mat[row]:
                if i == 1: sol += 1
            lst.append((sol, row))
            
        for i in sorted(lst)[:k]:
            s, r = i
            ans.append(r)
            
        return ans