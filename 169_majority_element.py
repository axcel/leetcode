class Solution:
    def majorityElement(self, nums: List[int]) -> int:
        map = {}
        for i in range(len(nums)):
            map[nums[i]] = map.get(nums[i], 0) + 1
            
        for k,v in map.items():
            if v > len(nums)/2:
                return k   