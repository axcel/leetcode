class Solution:
    def twoSum(self, numbers: List[int], target: int) -> List[int]:
        s = 0
        e = len(numbers) - 1
        flag = True
        ans = []
        
        while flag:
            
            if numbers[s] + numbers[e] == target:
                ans.append(s+1)
                ans.append(e+1)
                flag = False
            elif numbers[s] + numbers[e] > target:
                e -= 1
            else:
                s += 1
                
        return ans