class Solution {
    public int[] sortedSquares(int[] A) {
        int start = 0;
        int end = A.length-1;
        int[] arr = new int[A.length];
        int i = end;
        
        while(start <= end) {
            int pow1 = A[start]*A[start];
            int pow2 = A[end]*A[end];
            if(pow1 > pow2) {
                arr[i--] = pow1;
                start++;
            } else {
                arr[i--] = pow2;
                end--;
            }   
        }
        return arr;
    }
}