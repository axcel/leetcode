class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
 
def creatBTree(data, index=0):
    pNode = None
    if index < len(data):
        if data[index] == None:
            return
        pNode = TreeNode(data[index])
        pNode.left = creatBTree(data, 2 * index + 1) # [1, 3, 7, 15, ...]
        pNode.right = creatBTree(data, 2 * index + 2) # [2, 5, 12, 25, ...]
    return pNode 


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


def creatListNode(data):
    head = ListNode(data[0])
    prev = head
    for i in range(1, len(data)):
        prev.next = ListNode(data[i])
        prev = prev.next
    return head
