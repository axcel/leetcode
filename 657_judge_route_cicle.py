#  Initially, there is a Robot at position (0, 0).
#  Given a sequence of its moves, judge if this robot makes a circle,
#  which means it moves back to the original place.

#  The move sequence is represented by a string.
#  And each move is represent by a character.
#  The valid robot moves are R (Right), L (Left), U (Up) and D (down).
#  The output should be true or false representing whether the robot makes a circle. 

#  Example 1:
#  Input: "UD"
#  Output: true

#  Example 2:
#  Input: "LL"
#  Output: false


def judgeCircle(moves: str) -> bool:
    weight = {'U': 1, 'D': -1, 'R': 1, 'L': -1}
    UD = 0
    LR = 0
    for m in moves:
        if m.upper() in 'UD':
            UD += d[m]
        elif m.upper() in 'LR':
            LR += d[m]
        else:
            return False
    if UD == 0 and LR == 0:
        return True
    return False

