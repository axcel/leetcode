class Solution:
    def hammingDistance(self, x: int, y: int) -> int:
        
        ans = 0
        
        s_x = self.toTwo(x)
        s_y = self.toTwo(y)
        
        if len(s_x) != len(s_y):
            s_x, s_y = self.equalLen(s_x, s_y)
            
        for i in range(len(s_x)):
            if s_x[i] != s_y[i]:
                ans += 1
                
        return ans
        
        
    def toTwo(self, num: int) -> str:
        s = ''
        while (num >= 1):
            s += str(num%2)
            num //= 2
            
        return s[::-1]
    
    def equalLen(self, x: int, y: int):
        if len(x) > len(y):
            y = '0'*(len(x) - len(y)) + y
        if len(y) > len(x):
            x = '0'*(len(y) - len(x)) + x
        return x, y
            