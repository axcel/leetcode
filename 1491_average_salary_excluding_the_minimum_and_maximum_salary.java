class Solution {
    public double average(int[] salary) {
        if (salary.length < 3) {
            throw new IllegalArgumentException();
        }
        int max = salary[0];
        int min = salary[0];
        double sum = 0;
        for (int emp: salary) {
            if (max < emp) {
                max = emp;
            }
            if (min > emp) {
                min = emp;
            }
            sum += emp;
        }
        return (sum-max-min)/(salary.length-2);
    }
}