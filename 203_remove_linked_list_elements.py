# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def removeElements(self, head: ListNode, val: int) -> ListNode:
        curNode = ListNode(-1)
        curNode.next = head
        head = curNode
        
        while curNode.next:
            if curNode.next.val == val:
                curNode.next = curNode.next.next
            else:
                curNode = curNode.next
        return head.next