# Write a function that takes a string as input and reverse only the vowels of a string.
#  Example 1:
#  Input: "hello"
#  Output: "holle"
#  Example 2:
#  Input: "leetcode"
#  Output: "leotcede"

#  Note:
#  The vowels does not include the letter "y".


def reverseVowels(self, s: str) -> str:
    s = s.lower()
    s_vowels = [i for i in s if i in 'aeiou']
    return ''.join([s_vowels.pop() if i in 'aeiou' else i for i in s])
