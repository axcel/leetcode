class Solution:
    def balancedStringSplit(self, s: str) -> int:
        lcount = 0
        rcount = 0
        ans = 0
        
        for i in s:
            if i == 'R':
                rcount += 1
            if i == 'L':
                lcount += 1
                
            if rcount == lcount:
                ans += 1
        return ans