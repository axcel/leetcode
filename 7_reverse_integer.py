#Given a 32-bit signed integer, reverse digits of an integer.

#Example 1:
#Input: 123
#Output: 321

#Example 2:
#Input: -123
#Output: -321

#Example 3:
#Input: 120
#Output: 21

#Note:
#Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range: [−231,  231 − 1]. For the purpose of this problem, assume that your function returns 0 when the reversed integer overflows.

class Solution:
    def reverse(self, x: int) -> int:
        x_str = str(abs(x))
        x_list = x_str[:-1] if x_str[-1] == 0 else x_str
        x_ret = '-' + str(x_list[::-1]) if x < 0 else str(x_list[::-1])
        if(abs(int(x_ret)) > ((1 << 31) - 1)):
            return 0
        return int(x_ret)




class Solution:
    def reverse(self, x: int) -> int:
        result = 0
        q = abs(x)
        while (q != 0):
            result = result * 10 + q % 10
            q = q // 10
            
        maxa = 2**31 - 1
        
        if result >= maxa: return 0
            
        return -result if x < 0 else result