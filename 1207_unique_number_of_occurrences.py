class Solution:
    def uniqueOccurrences(self, arr: List[int]) -> bool:
        fMap = {}
        lstEl = []
        for i in arr:
            fMap[i] = fMap.get(i, 0) + 1

        for k,v in fMap.items():
            if v in lstEl:
                return False
            lstEl.append(v)
            
        return True         