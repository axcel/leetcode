//Given the root of a binary tree and an integer targetSum, return true if the tree has a root-to-leaf path such that adding up all the values along the path equals targetSum.

//A leaf is a node with no children.
//
//Input: root = [5,4,8,11,null,13,4,7,2,null,null,null,1], targetSum = 22
//Output: true
//
//


class Solution {
    public boolean hasPathSum(TreeNode root, int targetSum) {
        return search(root, 0, targetSum);
    }

    private boolean search(TreeNode root, int pathSum, int targetSum) {
        if (root == null) return false;

        if (root.left == null && root.right == null) {
            pathSum += root.val;
            if(pathSum == targetSum) return true;
            return false;
        }

        return search(root.left, pathSum+root.val, targetSum) || search(root.right, pathSum+root.val, targetSum);
    }
}
