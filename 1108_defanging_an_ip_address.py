class Solution:
    def defangIPaddr(self, address: str) -> str:
        dig_list = address.split('.')
        return '[.]'.join(dig_list)