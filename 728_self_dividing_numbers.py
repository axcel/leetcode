class Solution:
    def selfDividingNumbers(self, left: int, right: int) -> List[int]:
        if left == 0: return []
        ans = []
        for num in range(left, right+1):
            if self.checkDiv(num):
                ans.append(num)
        return ans
            
    def checkDiv(self, num):
        s = str(num)
        for k in s:
            if k == '0':
                return False
            if num % int(k) == 0: 
                continue
            else:
                return False
        return True