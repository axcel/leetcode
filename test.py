def maximum69Number (num: int) -> int:
    
    s_num = numToStr(num)
    
    for i in range(len(s_num)):
        if s_num[i] == '6':
            s_num = s_num[:i] + "9" + s_num[i+1:]
            
    return int(s_num)
    
    
def numToStr(num):
    s = ''
    while (num != 0):
        # num %= 10
        s = str(num % 10) + s
        num //= 10
        
    return s


print(maximum69Number(9669))