# Write a function that takes a string as input and returns the string reversed.

# Example 2:
# Input: "A man, a plan, a canal: Panama"
# Output: "amanaP :lanac a ,nalp a ,nam A"


def reverseString(s: str) -> str:
    rev = ''
    for i in s:
        rev = i + rev
    return rev


def reverseString(s: List[str]) -> None:
    """
    Do not return anything, modify s in-place instead.
    """
    len_s = len(s)//2
    for i in range(len_s):
        s[i], s[-(i+1)] = s[-(i+1)], s[i]
    