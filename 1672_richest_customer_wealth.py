class Solution:
    def maximumWealth(self, accounts: List[List[int]]) -> int:
        temp = 0
        ans = 0
        for account in accounts:
            for a in account:
                temp += a
            ans = max(ans, temp)
            temp = 0
        return ans