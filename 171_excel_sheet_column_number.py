import string

class Solution:
    def titleToNumber(self, columnTitle: str) -> int:
        alpha = string.ascii_uppercase
        map = {}
        res = 0
        
        for i in range(0, 26):
            map[alpha[i]] = i+1
        
        for i in range(len(columnTitle)):
            res += map.get(columnTitle[~i]) * pow(26, i)
        return res


s = Solution()
print(s.titleToNumber("A"))