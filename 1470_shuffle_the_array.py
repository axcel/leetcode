class Solution:
    def shuffle(self, nums: List[int], n: int) -> List[int]:
        ans = [0] * len(nums)
        k=0
        
        for i in range(len(nums)//2):
            ans[i+k], ans[i+k+1] = nums[i], nums[i+n]
            k += 1
        return ans