from typing import List


class Solution:
    def longestSubarray(self, nums: List[int]) -> int:
        start = 0
        freq_map = {}
        max_length = 0

        if set(nums).pop() == 0:
            return 0
        
        for end in range(len(nums)):
            right = nums[end]
            freq_map[right] = freq_map.get(right, 0) + 1
            
            if freq_map.get(0,0) >= 2:
                left = nums[start]
                freq_map[left] -= 1
                if freq_map.get(left) == 0:
                    del freq_map[left]

                start += 1
            max_length = max(max_length, end-start+1-1)
        
        if start == end: max_length = len(nums) - 1
        return max_length