from typing import List


class Solution:
    def repeatedNTimes(self, A: List[int]) -> int:
        map = {}
        for i in range(len(A)):
            map[A[i]] = map.get(A[i], 0) + 1
            if map.get(A[i]) != 1:
                return A[i]
        return -1