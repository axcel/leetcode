import math

class Solution:
    def isPalindrome(self, x: int) -> bool:

        if x < 0: return False
        if x == 0: return True

        numDigs = int(math.floor(math.log10(x) + 1))
        msdMask = 10 ** (numDigs - 1)
        for i in range(0, numDigs//2):
            if (int(x / msdMask) != x % 10):
                return False
            x %= msdMask
            x //= 10
            msdMask //= 100
        return True


s = Solution()

s.isPalindrome(151751)