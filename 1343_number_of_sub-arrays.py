class Solution:
    def numOfSubarrays(self, arr: List[int], k: int, threshold: int) -> int:
        start = 0
        ans = 0
        _sum = 0
        
        for end in range(len(arr)):
            _sum += arr[end]
            
            if end >= k-1:
                if _sum/k >= threshold:
                    ans += 1
                _sum -= arr[start]
                start += 1
                
        return ans