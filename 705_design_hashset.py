class MyHashSet:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.setMap = {}
        

    def add(self, key: int) -> None:
        self.setMap[key] = key
        return
        

    def remove(self, key: int) -> None:
        if key in self.setMap:
            self.setMap.pop(key)
        return
        

    def contains(self, key: int) -> bool:
        """
        Returns true if this set contains the specified element
        """
        if key in self.setMap:
            return True
        else: return False
        

obj = MyHashSet()
obj.add(9)
obj.remove(19)
obj.add(14)
obj.remove(19)
obj.remove(9)
obj.add
obj.add
obj.add
obj.add
obj.remove