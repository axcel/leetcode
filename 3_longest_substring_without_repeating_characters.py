class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        # if len(s) == 0:
        #     return 0

        # ans = 0
        # word = ''
        # uniq = set()
        # start = 0

        # for end in range(len(s)):
        #     char = s[end]
            
        #     while char in uniq:
        #         uniq.remove(s[start])
        #         start += 1

        #     uniq.add(char)

        #     ans = max(ans, end-start+1)

        if len(s) == 0:
            return 0

        ans = 0
        word = ''
        start = 0

        for end in range(len(s)):
            char = s[end]

            while char in word:
                word = word[1:]
                start +=1

            word += char
            ans = max(ans, end-start+1)
        return ans