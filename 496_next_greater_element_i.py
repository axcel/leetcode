class Solution:
    def nextGreaterElement(self, nums1: List[int], nums2: List[int]) -> List[int]:
        stack = []
        map = {}
        
        ans = []
        stack.append(nums2[0])
        for num in nums2[1:]:
            while stack and stack[-1] < num:
                map[stack.pop()] = num  
            stack.append(num)
            
        for num in nums1:
            ans.append(map.get(num, -1))
            
        return ans