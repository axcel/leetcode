from typing import List


def countConsistentStrings(allowed: str, words: List[str]) -> int:
    ans = 0
    flag = True
    for word in words:
        for i in set(word):
            if i not in allowed:
                flag = False
                break
            else:
                flag =True
        if flag:
            ans += 1
            
    return ans

print(countConsistentStrings("ab", ["ad","bd","aaab","baa","badab"]))