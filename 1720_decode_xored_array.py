from typing import List


def decode(encoded: List[int], first: int) -> List[int]:
    ans = []
    ans.append(first)
    
    for i in range(len(encoded)):
        d = ans[i]
        ans.append(encoded[i] ^ d)
        
    return ans

# more pretty solution
# def decode(encoded: List[int], first: int) -> List[int]:
#     ans = [first]
    
#     for i in range(len(encoded)):
#         # get last element
#         # d = ans[-1]
#         ans.append(encoded[i] ^ ans[-1])
        
#     return ans


print(decode([6,2,7,3], 4))