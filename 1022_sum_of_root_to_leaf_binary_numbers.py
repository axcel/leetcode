class Solution:
    def sumRootToLeaf(self, root: TreeNode) -> int:
        return self.dfs(root, 0)
    
    def dfs(self, root: TreeNode, amount: int) -> int:
        if root == None:
            return 0
        
        amount = 2 * amount + root.val

        if(root.left == None and root.right == None):
            return amount
        
        return self.dfs(root.left, amount) + self.dfs(root.right, amount)