def findAnagrams(s, p):

    p_value = getASCIIValue(p)
    ans = []

    for j in range(0, len(s) - len(p) + 1):
        check_str = s[j:j+len(p)]
        if sorted(check_str) == sorted(p):
        # if getASCIIValue(check_str) == p_value:
            ans.append(j)
    return ans


def getASCIIValue(k: str) -> int:
    v = 0
    for i in k:
        # if i not in p: return -1
        v += ord(i) - ord("a")
    return v

import time

start = time.time()
print(findAnagrams("cbaebabacd", "abc"))
print(findAnagrams("abab", "ab"))
print(findAnagrams("af", "be"))
print(time.time()-start)