from typing import List


class Solution:
    def countNegatives(self, grid: List[List[int]]) -> int:
        l = len(grid)
        ans = 0
        for i in range(l):
            for j in range(len(grid[i])):
                if grid[i][j] < 0:
                    ans += 1
                    
        return ans


s = Solution()
print(s.countNegatives([[5,1,0],[-5,-5,-5]]))