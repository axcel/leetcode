class Solution:
    def equalSubstring(self, s: str, t: str, maxCost: int) -> int:
        start, max_length, val = 0, 0, 0
        
        for end in range(len(s)):
            val += abs(ord(s[end])- ord(t[end]))
            
            while val > maxCost:
                val -= abs(ord(s[start])-ord(t[start]))
                start += 1
            
            max_length = max(max_length, end-start+1)
            
        return max_length

s = Solution()
s.equalSubstring("abcd", "bcdf", 3)