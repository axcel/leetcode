class Solution:
    def longestOnes(self, nums: List[int], k: int) -> int:
        start, max_one_score, max_length = 0, 0, 0
        
        for end in range(len(nums)):
            if nums[end] == 1:
                max_one_score += 1
                
            while (end-start + 1 - max_one_score) > k:
                if nums[start] == 1:
                    max_one_score -= 1
                start += 1
                
            max_length = max(max_length, end-start+1)
            
        return max_length