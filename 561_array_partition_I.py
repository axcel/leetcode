class Solution:
    def arrayPairSum(self, nums: List[int]) -> int:
        s_nums = sorted(nums)
        ans = 0
        for i in range(0, len(nums), 2):
            # список отсортирован и мимальное значение по первому элементу
            ans += s_nums[i]
            # ans += min(s_nums[i], s_nums[i+1])
        return ans