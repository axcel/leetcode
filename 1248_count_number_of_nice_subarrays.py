class Solution:
    def numberOfSubarrays(self, nums: List[int], k: int) -> int:
        start = 0
        count_odd = 0
        _sum = 0
        max_length = 0
        
        for end in range(len(nums)):

            if nums[end] % 2 != 0:
                count_odd += 1
                _sum = 0
                
            while count_odd >= k:
                if nums[start] % 2 != 0:
                    count_odd -= 1
                start += 1
                _sum += 1
                
            max_length += _sum
            
        return max_length