from typing import List


class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        freqMap = {}
        
        for i in nums:
            freqMap[i] = freqMap.get(i, 0) + 1
            if freqMap.get(i) >= 2:
                return True
        return False


# or 

# return len(nums) != len(set(nums))