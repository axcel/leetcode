class Solution {
    public int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> set1 = new HashSet<>();
        Set<Integer> set2 = new HashSet<>();
        
        for(int i: nums1) {
            set1.add(i);
        }

        for (int num: nums2) {
            if(set1.contains(num)) set2.add(num);
        }
        
        int k = 0;
        int[] result = new int[set2.size()];
        for(int i: set2) {
            result[k++] = i;
        }
        return result;
    }
}