class Solution {
    public List<List<String>> groupAnagrams(String[] strs) {
        if (strs == null) return new ArrayList();
        HashMap<Integer, List> map = new HashMap<Integer, List>();
        for(String str: strs) {
            char[] ar = str.toCharArray();
            Arrays.sort(ar);
            String word = String.valueOf(ar);
            
            map.putIfAbsent(word.hashCode(), new ArrayList());
            map.get(word.hashCode()).add(str);
        }
        return new ArrayList(map.values());
    }
}