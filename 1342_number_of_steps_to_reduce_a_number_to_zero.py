def numberOfSteps (num: int) -> int:
    ans = 0
    while(num != 0):
        num = num / 2 if num % 2 == 0 else num - 1
        ans += 1
    return ans

print(numberOfSteps(123))


def numberOfSteps1 (self, num: int) -> int:
    ans = 0
    while(num > 0):
        if num % 2 == 0:
            num //=2
        else:
            num -= 1
        ans += 1
    return ans