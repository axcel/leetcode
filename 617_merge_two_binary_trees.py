from TreeNode import creatBTree

class Solution:
    def mergeTrees(self, root1, root2):
        
        if root1 == None:
            return root2
        if root2 == None:
            return root1
        
        root1.val = root1.val + root2.val
        root1.left = self.mergeTrees(root1.left, root2.left)
        root1.right = self.mergeTrees(root1.right, root2.right)
        return root1


s = Solution()

t1 = [1,3,2,5]
t2 = [2,1,3,None,4,8,None,7]

root1 = creatBTree(t1)
root2 = creatBTree(t2)

print(s.mergeTrees(root1, root2))