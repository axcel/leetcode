from typing import List


def findNumbers(nums: List[int]) -> int:
    ans = 0
    for i in range(len(nums)):
        if not countDigits(nums[i]):
            ans += 1
    return ans

def countDigits(num):
    count = 0
    while num != 0:
        num //= 10
        count += 1
    return count%2


print(findNumbers([555,901,482,1771]))