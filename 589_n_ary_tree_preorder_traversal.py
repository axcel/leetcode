class Solution:
    def preorder(self, root: 'Node') -> List[int]:
        
        if root == None:
            return
        
        ans = []
        ans.append(root.val)
        for child in root.children:
            ans.extend(self.preorder(child))
        
        return ans


class Solution:
    def preorder(self, root: 'Node') -> List[int]:
        stack = []
        stack.append(root)
        ans = []
        
        while(len(stack) != 0):
            node = stack.pop()
            if (node != None):
                for child in node.children[::-1]:
                    stack.append(child)
                ans.append(node.val)
        return ans