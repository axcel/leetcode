class Solution:
    def numUniqueEmails(self, emails: List[str]) -> int:
        ans = set()
        
        for email in emails:
            prefix, postfix = email.split('@')
            before = prefix.split('+')
            n_email = before[0].replace('.', '') + "@" + postfix
            ans.add(n_email)
            
        return len(ans)