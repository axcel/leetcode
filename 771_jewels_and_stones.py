# https://leetcode.com/problems/jewels-and-stones/description/

# You're given strings J representing the types of stones that are jewels,
# and S representing the stones you have.  Each character in S is a type of stone you have.
# You want to know how many of the stones you have are also jewels.
# The letters in J are guaranteed distinct, and all characters in J and S are letters.
# Letters are case sensitive, so "a" is considered a different type of stone from "A".

# Example
# Input: J = "aA", S = "aAAbbbb"
# Output: 3

def numJewelsInStones(J:str, S:str) -> int:
    count = 0
    for i in J:
        count += S.count(i)
    return count


    def numJewelsInStones(self, jewels: str, stones: str) -> int:
        if len(jewels) < 1: return 0
        
        map = {}
        ans = 0
        for stone in stones:
            if stone in map.keys():
                map[stone] += 1
            else:
                map[stone] = 1

        for i in jewels:
            if i in map.keys():
                ans += map.get(i)
        return ans