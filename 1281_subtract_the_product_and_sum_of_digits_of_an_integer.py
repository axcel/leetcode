class Solution:
    def subtractProductAndSum(self, n: int) -> int:
        sum = 0
        prod = 1
        
        for i in str(n):
            sum += int(i)

        for j in str(n):
            prod *= int(j)

        return prod - sum