def islandPerimeter(grid):
    rows = len(grid)
    cols = len(grid[0])
    ans = 0
    
    for i in range(rows):
        for j in range(cols):
                
            if grid[i][j] == 1:
                ans += 4
                if i > 0: 
                    ans -= grid[i-1][j]
                if j > 0: 
                    ans -= grid[i][j-1]
                if j < cols-1: 
                    ans -= grid[i][j+1]
                if i < rows-1: 
                    ans -= grid[i+1][j]
                    
    return ans


grid = [[0,1,0,0],[1,1,1,0],[0,1,0,0],[1,1,0,0]]
print(islandPerimeter(grid))
