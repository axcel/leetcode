def maximum69Number (num: int) -> int:
    # max69 = num
    # nmax = 0
    str_num = str(num)

    for i in range(len(str_num)):
        if str_num[i] == '6':
            return int(str_num[:i] + '9' + str_num[i+1:])
    return num

    s= list(str(num))
    for i in range(len(s)):
        if s[i] == '6':
            s[i] = '9'
            break
    return int(''.join(s))


print(maximum69Number(9669))