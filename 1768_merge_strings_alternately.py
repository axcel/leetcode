def mergeAlternately(word1: str, word2: str) -> str:
    ans = ''
    if len(word1) > len(word2):
        for i in range(len(word2)):
            ans += word1[i] + word2[i]
        ans += word1[len(word2):]
    else: 
        for i in range(len(word1)):
            ans += word1[i] + word2[i]
        ans += word2[len(word1):]
        
    return ans

# print(mergeAlternately("abc", "pqr"))
# print(mergeAlternately("ab", "pqrs"))
print(mergeAlternately("abcd", "pq"))
# print(mergeAlternately("cdf", "a"))