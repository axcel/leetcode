class Solution {
    public String restoreString(String s, int[] indices) {
        String nstr="";
        HashMap<Integer, Character> map = new HashMap<>();
        for (int i=0; i<indices.length; i++) {
            map.put(indices[i], s.charAt(i));
        }
        
        for (Map.Entry<Integer, Character> entry: map.entrySet()) {
            nstr += entry.getValue();
        }
        return nstr;
    }
}