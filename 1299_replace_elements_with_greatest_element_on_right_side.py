def replaceElements(arr):
    ans = []
    for i in range(0, len(arr)):
        if i+1 > len(arr)-1:
            ans.append(-1)
        else:
            ans.append(max(arr[i+1:]))
        
    # ans.append(-1)
    return ans


print(replaceElements([17,18,5,4,6,1]))