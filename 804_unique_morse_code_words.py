#  International Morse Code defines a standard encoding where each letter
#  is mapped to a series of dots and dashes, as follows:
#  "a" maps to ".-", "b" maps to "-...", "c" maps to "-.-.", and so on.

#  Now, given a list of words, each word can be written as a concatenation of
#  the Morse code of each letter. For example, "cab" can be written as "-.-.-....-",
#  (which is the concatenation "-.-." + "-..." + ".-").
#  We'll call such a concatenation, the transformation of a word.

#  Return the number of different transformations among all words we have.


#  Example:
#  Input: words = ["gin", "zen", "gig", "msg"]
#  Output: 2
#  Explanation:
#  The transformation of each word is:
#  "gin" -> "--...-."
#  "zen" -> "--...-."
#  "gig" -> "--...--."
#  "msg" -> "--...--."

#  There are 2 different transformations, "--...-." and "--...--.".

#  Note:
    #  The length of words will be at most 100.
    #  Each words[i] will have length in range [1, 12].
    #  words[i] will only consist of lowercase letters.


def uniqueMorseRepresentations(self, words -> List) -> int:
        morse = [".-","-...","-.-.","-..",".","..-.","--.","....","..",
                 ".---","-.-",".-..","--","-.","---",".--.","--.-",".-.",
                 "...","-","..-","...-",".--","-..-","-.--","--.."]
        lower = 'abcdefghijklmnopqrstuvwxyz'
        unique = set()
        if len(words) > 100:
            return
        for word in words:
            if len(word) > 12:
                pass
            morse_word = ''.join([morse[lower.index(i)] for i in word])
            unique.add(morse_word)
        return len(unique)
