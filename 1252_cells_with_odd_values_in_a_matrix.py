class Solution:
    def oddCells(self, n: int, m: int, indices: List[List[int]]) -> int:
        ans = 0
        matrix = [[0]*m for i in range(n)]

        for index in indices:
            r_i = index[0]
            c_i = index[1]
            
            for i in range(m):
                matrix[r_i][i] += 1

            for j in range(n):
                matrix[j][c_i] += 1

        for t in range(len(matrix)):
            for k in matrix[t]:
                if k % 2 == 1:
                    ans +=1
        
        return ans