def interpret(command: str) -> str:
    ans = ''
    tmp = []
    for i in command:
        if i == '(':
            tmp.append(i)
            continue
        if tmp:
            tmp.append(i)
        else:
            ans += i
        if i == ')':
            # tmp.append(i)
            if len(tmp) == 2: 
                ans += 'o'
            else:
                 ans += 'al'

            tmp.clear()

    return ans

print(interpret("G()(al)"))

print(interpret("G()()()()(al)"))

print(interpret("(al)G(al)()()G"))