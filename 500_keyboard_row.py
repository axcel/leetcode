class Solution:
    def findWords(self, words: List[str]) -> List[str]:

        ans = []      
            
        for word in words:
            count = 0
            row = self.getCharRow(word)
            
            for i in word:
                if i.lower() in row:
                    count += 1
                    
            if count == len(word):
                ans.append(word)

        return ans
    
    def getCharRow(self, word):
        
        check_row = ''
        
        first_row = "qwertyuiop"
        second_row = "asdfghjkl"
        third_row = "zxcvbnm"
        
        if word[0].lower() in first_row:
            check_row = first_row
        elif word[0].lower() in second_row:
            check_row = second_row
        else:
            check_row = third_row
            
        return check_row