def maxDepth(self, root: 'Node') -> int:

    if root == None:
        return 0
    
    ans = 0
    for i in range(len(root.children)):
        ans = max(self.maxDepth(root.children[i]), ans)
    
    return ans+1