class Solution {
    public int step = 0;
    public int numberOfSteps (int num) {
        
        if (num % 2 != 0) {
            step++;
            num--;
        }
        
        int res = num / 2;

        if (res == 0) return 0;
        step++;
        
        numberOfSteps(res);
        return step;
    }

}