class Solution:
    def runningSum(self, nums: List[int]) -> List[int]:
        ans = []
        for i in nums:
            if len(ans) == 0:
                ans.append(i)
            else:
                num = (ans[-1] + i)
                ans.append(num)
        return ans
        # ugly solution, check java solution for more pretty