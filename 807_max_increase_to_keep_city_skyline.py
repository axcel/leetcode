class Solution:
    def maxIncreaseKeepingSkyline(self, grid: List[List[int]]) -> int:
        top = []
        left = []
        
        for i in range(0, len(grid[0])):
            n = grid[0][i]
            # print(n)
            left.append(max(grid[i]))
            for j in range(0, len(grid)):
                n = max(n, grid[j][i])
            top.append(n)
            
        res = 0
        for i in range(0, len(grid)):
            for j in range(0, len(grid[i])):
                pos = min(left[i], top[j])
                # if pos > grid[i][j]:
                    # print("if here")
                res += pos - grid[i][j]
        
        return res